'use strict';


window.securemailApp = {
  constants: angular.module('securemailApp.constants', []),
  services: angular.module('securemailApp.services', []),
  controllers: angular.module('securemailApp.controllers', []),
  filters: angular.module('securemailApp.filters', []),
  directives: angular.module('securemailApp.directives', [])
};

angular
  .module('securemailApp', [
    'ngResource',
    'ui.router',
    'ui.select2',
    'securemailApp.services',
    'securemailApp.controllers',
    'securemailApp.directives'


  ])
  .config(function($locationProvider, $stateProvider) {

    $locationProvider.html5Mode(true);


    $stateProvider
      .state('inbox', {
        url: '/',
        templateUrl: 'views/inbox.html',
        controller: 'InboxCtrl'
      })
      .state('sent', {
        url: '/sent'
      })
      .state('drafts', {
        url: '/drafts'
      })
      .state('bin', {
        url: '/bin'
      })
      .state('new', {
        url: '/new',
        templateUrl: 'views/new.html',
        controller: 'NewCtrl'
      })
      .state('read', {
        url: '/read/:id',
        templateUrl: 'views/read.html',
        controller: 'ReadCtrl'
      })

      ;
  })
  .run(function($rootScope) {

      $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
        console.log('Previous state:'+$rootScope.previousState)
        console.log('Current state:'+$rootScope.currentState)
    });
  });
