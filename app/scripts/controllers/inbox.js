

window.securemailApp.controllers

  .controller('InboxCtrl', function ($stateParams, $scope, InboxService) {

      var PAGE_SIZE = 10;
      $scope.currentPage = 1 ;


      $scope.selection = [];
      $scope.selectAll = false;


      $scope.changePage = function(p) {

        getPage(p, function(inbox) {
          $scope.inbox = inbox;
          $scope.currentPage = p;
        });
      }

      $scope.changePage(1);

      $scope.list = {
        isSelected : true
      };


      $scope.delete = function() {

        console.log('deleting emails: ', $scope.selection);
      };


      $scope.refresh = function() {
        getPage($scope.currentPage, function(inbox) {
          $scope.inbox = inbox;
        });
      };


      $scope.toggleSelection = function toggleSelection(emailId) {
        var idx = $scope.selection.indexOf(emailId);

        // is currently selected
        if (idx > -1) {
          $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
          $scope.selection.push(emailId);
        }


      };


      $scope.toggleSelectAll = function() {

        // TODO: should be done in directive

        if($scope.selectAll) {
          $scope.selectAll = false;

          $scope.selection = [];

        } else {
          angular.forEach($scope.inbox, function (email) {
            $scope.selection.push(email.id);
          });

          $scope.selectAll = true;
        }

        console.log($scope.selection);


      };


      function getPage(i, cb) {

        InboxService.all(function(emails) {
            var x = (i -1) * PAGE_SIZE ;
            $scope.pages = Math.ceil(emails.length / 10) ;

            cb(emails.slice(x, x + PAGE_SIZE));
        });


      }
  })



  .controller('ReadCtrl', function($stateParams, $scope, $rootScope, InboxService) {


    InboxService.all(function(emails) {

      var res = emails.filter(function(email) {
        return email.id == $stateParams.id;
      });

      $scope.email = res[0];


    });

    $scope.back = $rootScope.previousState;



  });
