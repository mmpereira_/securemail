



window.securemailApp.services.factory('InboxService', function($resource) {

  return $resource('/data/inbox.json', {}, {
      all: {
        cache: true,
        isArray: true
      }
    });
});
