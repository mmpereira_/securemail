window.securemailApp.services.factory('ContactService', function($resource) {

  return $resource('/data/contacts.json', {}, {
      all: {
        cache: true,
        isArray: true
      }
    });
});
